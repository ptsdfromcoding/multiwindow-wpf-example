﻿using MultiWindowTest.ViewModels;
using MultiWindowTest.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiWindowTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CharacterHandler parentCharHandler;

        public MainWindow()
        {
            InitializeComponent();

            parentCharHandler = new CharacterHandler();

            DataContext = parentCharHandler;
        }

        private void NewCharButton_Click(object sender, RoutedEventArgs e)
        {
            NewCharacterWindow myWindow = new NewCharacterWindow();
            myWindow.Owner = this;
            myWindow.DataContext = parentCharHandler;
            myWindow.childCharHandler = parentCharHandler;
            myWindow.Show();
        }
    }
}
