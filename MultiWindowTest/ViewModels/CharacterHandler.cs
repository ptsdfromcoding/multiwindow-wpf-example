﻿using MultiWindowTest.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MultiWindowTest.ViewModels
{
    public class CharacterHandler : INotifyPropertyChanged
    {
        private Character _newCharacter;

        public ObservableCollection<Character> Characters { get; set; }
        public Character NewCharacter {
            get
            {
                return _newCharacter;
            }
            set
            {
                _newCharacter = value;
                OnPropertyChanged("NewCharacter");
            }
        }

        public CharacterHandler()
        {
            Characters = new ObservableCollection<Character>()
            {
                new Character() { Name = "Женя Серебряков", Class="Воин", Level = 1 },
                new Character() { Name = "Артем Михайлов", Class = "Маг", Level = 2 }
            };

            NewCharacter = new Character();
        }

        public void AddCharacter()
        {
            Characters.Add(NewCharacter);
            NewCharacter = new Character();
        }

        //Служебное

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
    }
}
