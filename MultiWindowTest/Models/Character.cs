﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiWindowTest.Models
{
    public class Character
    {
        private String _name;
        private String _class;
        private UInt32 _level;

        public String Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public String Class
        {
            get
            {
                return _class;
            }
            set
            {
                _class = value;
            }
        }
        public UInt32 Level
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
            }
        }
    }
}
